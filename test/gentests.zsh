#!/bin/zsh
local TESTNAME
for file in *.data
do
    TESTNAME=$file:t:r
    cat > $TESTNAME.in << EOF
:call DoTest1()
EOF
    if [[ ${file:t:r:e} == invalid ]] ; then
        touch $TESTNAME.ok
    fi
done

function repeat()
{
    local -i I=$2
    while ((I)) ; do
        echo -n $1
    done
}
