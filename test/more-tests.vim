execute frawor#Setup('0.0', {'@%yaml': '1.0'})
let l=[]
call add(l, l)
let strings=["", ":abc", ': abc', '- abc', "ab: c", "ab\nc", "ab\rc",
            \"[abc", "{abc", "[abc]", "{ab:c}", "\"abc\"", "\"abc", "'abc",
            \"'abc'", "ab'c", "ab\"c", "ab[c", "ab]c", "ab{c", "ab}c",
            \'!', '-!', '-', '|', '?', '>', '1', '# ', '# abc', '&a', '& ',
            \'*a', '&', '*', '* ', 'a*a', 'a*', 'a&', 'a&a', '? ', 'a? ',
            \"a?\n", 'a?a', 'a?', '%a', '% a', 'a%', 'a%a', '=', '<<', '=a',
            \'<<a', "\u2028", "\u2029", "\u0085", "ab:c", "́", "á",
            \"true", "false", "~", "null", "\x85",
            \repeat("\naaa ", 30),
            \repeat("\naaa", 30),
            \repeat("aaa ", 30),
            \repeat("aaa", 29)."aaa",
            \repeat("ab:c ", 30)."ab:c",
            \repeat("aaa ", 30)."a\n  aa".repeat("aaa ", 30),
            \"\x0B\x08".repeat("abc def", 30),
            \"\x0B".repeat("abc  def", 30),
            \"\x0B".repeat("  abc  def  ", 30),
            \repeat("  abc  def  ", 30),
            \repeat("  abc  def  ", 30)."a\n",
            \repeat("  abc  def  ", 30)."a",
            \"a".repeat("  abc  def  ", 30),
            \"a".repeat("  abc  def  ", 30)."a",
            \repeat(repeat("abc def ", 10)."a\n", 10),
            \repeat(repeat("abc def ", 10)."a", 10),
            \repeat(repeat("abc\tdef ", 10)."a", 10),
            \repeat(repeat("abc\tdef\t", 10)."a", 10),
            \repeat(repeat(" \t ", 10)."a", 10),
            \repeat(repeat("abc def ", 10)."\n", 10),]
let numbers=[0, -1, 1, 1.0, 0.0, -1.0, 1.0e10, 1.0e-10, 1.0e50000, -1.0e500000]
let dict={}
let mdicts=[]
call map(strings[1:], 'extend(dict, {v:val : v:val})')
call map(strings[1:], 'add(mdicts, {v:val : v:val})')
let dict2={}
let dict2.self=dict2
let dict2["\xFF"]="abc"
let dict2["'"]="def"
let dict2['"']="ghi"
let dict3=deepcopy(dict2)
let dict3.list=l
let dict4={"t": dict3, "l": deepcopy(l)}
call add(dict4.l, dict4)
let l2=deepcopy(l)
call add(l2, dict4)
let fnames=['empty', 'strlen', 'synconcealed', 'match', 'writefile', 
            \'fnamemodify', 'add', 'range', 'getloclist', 'getcmdtype', 'server2client', 
            \'synIDtrans', 'settabvar', 'strpart', 'join', 'bufexists', 'taglist', 'items', 
            \'argc', 'expand', 'sort', 'strftime', 'pow', 'cscope_connection', 'substitute', 
            \'asin', 'inputsecret', 'feedkeys', 'mkdir', 'abs', 'line', 'localtime', 
            \'lispindent', 'findfile', 'winwidth', 'spellsuggest', 'getwinposx', 
            \'filewritable', 'extend', 'winline', 'remote_expr', 'float2nr', 'settabwinvar', 
            \'getftime', 'repeat', 'system', 'matchadd', 'getfperm', 'shellescape', 
            \'undofile', 'inputdialog', 'str2nr', 'clearmatches', 'getwinposy', 'strtrans', 
            \'keys', 'gettabvar', 'ceil', 'tagfiles', 'synIDattr', 'exists', 'histget', 
            \'getcwd', 'visualmode', 'bufwinnr', 'exp', 'stridx', 'hasmapto', 'matchlist', 
            \'acos', 'matcharg', 'undotree', 'matchend', 'complete_check', 'call', 
            \'serverlist', 'nextnonblank', 'sinh', 'histdel', 'diff_filler', 'remote_read', 
            \'mode', 'isdirectory', 'count', 'index', 'submatch', 'round', 'getchar', 
            \'complete_add', 'matchdelete', 'values', 'getpos', 'inputsave', 'searchdecl', 
            \'winbufnr', 'function', 'setpos', 'tanh', 'cindent', 'complete', 'getcmdpos', 
            \'string', 'hlID', 'maparg', 'garbagecollect', 'fmod', 'winsaveview', 'min', 
            \'remote_send', 'hostname', 'getcmdline', 'tr', 'setwinvar', 'confirm', 
            \'strchars', 'reltimestr', 'getbufvar', 'deepcopy', 'char2nr', 'cursor', 
            \'foreground', 'reverse', 'sin', 'gettabwinvar', 'get', 'tabpagenr', 'input', 
            \'filter', 'histnr', 'filereadable', 'tabpagewinnr', 'histadd', 'islocked', 
            \'searchpos', 'pathshorten', 'bufnr', 'remove', 'append', 'getpid', 'reltimestr', 
            \'getbufvar', 'deepcopy', 'char2nr', 'cursor', 'foreground', 'reverse', 'sin', 
            \'gettabwinvar', 'get', 'tabpagenr', 'input', 'filter', 'histnr', 'filereadable', 
            \'tabpagewinnr', 'histadd', 'islocked', 'searchpos', 'pathshorten', 'bufnr', 
            \'remove', 'append', 'getpid', 'search', 'buflisted', 'insert', 'sqrt', 
            \'soundfold', 'atan', 'inputlist', 'finddir', 'foldlevel', 'readfile', 
            \'fnameescape', 'getqflist', 'virtcol', 'foldtext', 'globpath', 'reltime', 
            \'printf', 'did_filetype', 'getfontname', 'getline', 'byteidx', 'tan', 
            \'setmatches', 'col', 'cos', 'glob', 'searchpair', 'strridx', 'floor', 
            \'diff_hlID', 'getreg', 'winrestview', 'bufname', 'escape', 'libcallnr', 
            \'setqflist', 'trunc', 'winnr', 'executable', 'bufloaded', 'winheight', 'has', 
            \'getcharmod', 'eventhandler', 'getregtype', 'winrestcmd', 'setreg', 'line2byte', 
            \'type', 'len', 'inputrestore', 'setline', 'synstack', 'getfsize', 'setcmdpos', 
            \'copy', 'str2float', 'getftype', 'iconv', 'getbufline', 'setbufvar', 'strwidth', 
            \'matchstr', 'spellbadword', 'mapcheck', 'delete', 'split', 'cosh', 'getwinvar', 
            \'rename', 'tabpagebuflist', 'synID', 'changenr', 'remote_peek', 'setloclist', 
            \'wincol', 'resolve', 'log10', 'eval', 'prevnonblank', 'has_key', 'argv', 
            \'byte2line', 'foldclosed', 'log', 'simplify', 'searchpairpos', 
            \'strdisplaywidth', 'tempname', 'atan2', 'argidx', 'foldtextresult', 'browse', 
            \'nr2char', 'getmatches', 'tolower', 'map', 'max', 'pumvisible', 'haslocaldir', 
            \'hlexists', 'libcall', 'browsedir', 'remote_foreground', 'foldclosedend', 
            \'toupper', 'indent', 'FixedOffset']
call filter(fnames, "exists('*'.v:val)")
" let fnames=fnames[:10]
let fdict={}
call map(copy(fnames), 'extend(fdict, {v:val : function(v:val)})')
let dumps=strings+numbers+values(fdict)+mdicts+[
            \{}, dict, {"t": l}, dict2, dict3, dict4, fdict,
            \[], strings, l, l2, numbers,
        \]
" finish
" call remove(dumps, 0, 325)
lockvar 2 dumps
let i=0
function! s:AlreadyChecked(checked, obj)
    if type(a:obj)!=type([]) && type(a:obj)!=type({})
        return 0
    endif
    for o in a:checked
        if o is a:obj
            return 1
        endif
    endfor
    call add(a:checked, a:obj)
    return 0
endfunction
function! s:GetKey(str)
    if len(a:str)>16
        return strtrans(a:str[:10]).'...'.a:str[-3:]
    else
        return strtrans(a:str)
    endif
endfunction
function! FindDifference(obj1, obj2, context, checked)
    if type(a:obj1)!=type(a:obj2)
        echom a:context.":Different types"
        return 0
    endif
    if s:AlreadyChecked(a:checked, a:obj1)
        return 1
    endif
    let r=1
    let tobj=type(a:obj1)
    if tobj==type("") || tobj==type(function("tr"))
        return a:obj1==#a:obj2
    elseif tobj==type(0) || tobj==type(0.0)
        return a:obj1==a:obj2
    elseif tobj==type({})
        if len(keys(a:obj1))!=len(keys(a:obj2))
            let r=0
            echom a:context.":Number of keys differ"
        endif
        for [key, l:Value1] in items(a:obj1)
            if !has_key(a:obj2, key)
                echom a:context.":Key absent:".s:GetKey(key)
                unlet l:Value1
                let r=0
                continue
            endif
            let l:Value2=a:obj2[key]
            let context=a:context."/".strtrans(key[:10])
            if !FindDifference(l:Value1, l:Value2, context, a:checked)
                let r=0
            endif
            unlet l:Value2 l:Value1
        endfor
    elseif tobj==type([])
        let idx=0
        let lobj2=len(a:obj2)
        let lobj1=len(a:obj1)
        if lobj2!=lobj1
            let r=0
            echom a:context.":Length differ:".lobj1."/=".lobj2
        endif
        for l:Value1 in a:obj1
            if idx>=lobj2
                echom a:context.":Index absent:".idx
                unlet l:Value1
                let r=0
                continue
            endif
            let l:Value2=a:obj2[idx]
            let context=a:context."/".idx
            if !FindDifference(l:Value1, l:Value2, context, a:checked)
                let r=0
            endif
            unlet l:Value2 l:Value1
            let idx+=1
        endfor
    endif
    return r
endfunction
for D in dumps
    try
        " call system("echo ".i.". > cur")
        let reference=s:_r.yaml.dumps(D, 1, {'key_sort': 1})
        let dumped=s:_r.yaml.dumps(D)
        let L=s:_r.yaml.loads(dumped)
        let dloaded=s:_r.yaml.dumps(L, 1, {'key_sort': 1})
        if type(L)!=type(D) || dloaded!=#reference
            throw 'Check 1 failed'
        endif
        unlet L
        " call system("echo ".i.".. > cur")
        let dumped=s:_r.yaml.dumps(D, 1, {'preserve_locks': 1})
        let L=s:_r.yaml.loads(dumped)
        let dloaded=s:_r.yaml.dumps(L, 1, {'key_sort': 1})
        if type(L)!=type(D) || dloaded!=#reference
            throw 'Check 2 failed'
        endif
        unlet L
        " call system("echo ".i."... > cur")
        let dumped=s:_r.yaml.dumps(D, 1, {'key_sort': 1})
        let L=s:_r.yaml.loads(dumped)
        let dloaded=s:_r.yaml.dumps(L, 1, {'key_sort': 1})
        if type(L)!=type(D) || dloaded!=#reference
            throw 'Check 3 failed'
        endif
        unlet L
        " call system("echo ".i.".... > cur")
        let dumped=s:_r.yaml.dumps(D, 1, {'preserve_locks': 1, 'key_sort': 1})
        let L=s:_r.yaml.loads(dumped)
        let dloaded=s:_r.yaml.dumps(L, 1, {'key_sort': 1})
        if type(L)!=type(D) || dloaded!=#reference
            throw 'Check 4 failed'
        endif
        unlet L
        " call system("echo ".i."..... > cur")
        let dumped=s:_r.yaml.dumps(D, 1, {'all_flow': 1})
        let L=s:_r.yaml.loads(dumped)
        let dloaded=s:_r.yaml.dumps(L, 1, {'key_sort': 1})
        if type(L)!=type(D) || dloaded!=#reference
            throw 'Check 5 failed'
        endif
        unlet L
        " call system("echo ".i."...... > cur")
        let dumped=s:_r.yaml.dumps(D, 1, {'all_flow': 1, 'preserve_locks': 1})
        let L=s:_r.yaml.loads(dumped)
        let dloaded=s:_r.yaml.dumps(L, 1, {'key_sort': 1})
        if type(L)!=type(D) || dloaded!=#reference
            throw 'Check 6 failed'
        endif
        unlet L
    catch
        execute 'redir! > '.g:curtest.'.fail'
        echo 'For test number '.i
        echo 'Caught exception: '.v:exception
        echo 'D:'
        echo D
        echo 'dumped:'
        echo dumped
        echo 'L:'
        echo L
        echo 'dloaded:'
        echo dloaded
        echo 'reference:'
        echo reference
        call FindDifference(L, D, '/', [])
        redir END
        redir! > dumped.yaml
        silent echo dumped
        redir END
        redir! > dloaded.yaml
        silent echo dloaded
        redir END
        redir! > reference.yaml
        silent echo reference
        redir END
        break
    endtry
    let i+=1
    unlet D
endfor
